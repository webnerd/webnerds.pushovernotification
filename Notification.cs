﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace WebNerds.PushoverNotification
{
    public static class Notification
    {
        public static bool Send(string apiKey, string userKey, string message, string title = null, string url = null, string url_title = null, bool timestamp = false, PushoverSound sound = PushoverSound.pushover)
        {
            try
            {
                NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);

                queryString["token"] = apiKey;
                queryString["user"] = userKey;
                queryString["message"] = message;

                if (title != null)
                    queryString["title"] = title;

                if (url != null)
                    queryString["url"] = url;

                if (url_title != null)
                    queryString["url_title"] = url_title;

                if (timestamp == true)
                    queryString["timestamp"] = DateTime.Now.DateTimeToUnixTimestamp().ToString();

                if (sound != PushoverSound.pushover)
                    queryString["sound"] = sound.ToString();

                var request = (HttpWebRequest)WebRequest.Create("https://api.pushover.net/1/messages");
                request.Method = "POST";
                request.Accept = "*/*";

                string postData = queryString.ToString();
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }
                
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(dataStream))
                        {
                            string responseFromServer = reader.ReadToEnd();
                        }
                    }
                }
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static long DateTimeToUnixTimestamp(this DateTime _DateTime)
        {
            TimeSpan _UnixTimeSpan = (_DateTime - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)_UnixTimeSpan.TotalSeconds;
        }
    }
}