﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebNerds.PushoverNotification
{
    public enum PushoverSound
    {
        pushover,
        bike,
        bugle,
        cashregister,
        classical,
        cosmic,
        falling,
        gamelan,
        incoming,
        intermission,
        magic,
        mechanical,
        pianobar,
        siren,
        spacealarm,
        tugboat,
        none
    }
}