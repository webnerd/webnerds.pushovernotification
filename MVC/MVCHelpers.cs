﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebNerds.PushoverNotification.MVC
{
    public static class MVCHelpers
    {
        public static bool SendNotification(this HtmlHelper helper, string apiKey, string userKey, string message, string title = null, string url = null, string url_title = null, bool timestamp = false, PushoverSound sound = PushoverSound.pushover)
        {
            return Notification.Send(apiKey, userKey, message, title: title, url: url, url_title: url_title, timestamp: timestamp, sound: sound);
        }
    }
}